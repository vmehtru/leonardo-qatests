module.exports = {
	elements : {
		
		totalScoreInput : {
			selector : 'div.score-heading > input'
		},

		totalScoreInDescription : {
			selector : 'div.instruction2 > b:nth-child(1)'
		},

		tableRows : {
			selector : 'div.table-row'
		},

		feedbackTextArea : {
			selector : 'textarea.feedback-textarea'
		},

		hintInputArea : {
			selector : 'div.hint-input'
		},

		penaltyInputArea : {
			selector : 'input.penalty-input'
		},

		addNewRule : {
			selector : 'div.addRuleButton'
		},

		addNewCell : {
			selector : 'div.addRuleForm div.range input'
		},

		addNewScore : {
			selector : 'div.addRuleForm div.score input'
		},

		initialDocument : {
			selector : 'a.initial-doc'
		},

		finalDocument : {
			selector : 'a.final-doc'
		}		
	},

	commands : [

	{

		toggleScore: function(rowNumber) {
			try {
				var temp = this 		
				this
					.api.elements('css selector', 'div.toggle-rule > i', function(result) {
						temp.api.elementIdClick(result.value[rowNumber - 1].ELEMENT, function() {
							temp
								.api.elements('css selector', 'div.table-row', function(result2) {
									temp.api.elementIdAttribute(result2.value[rowNumber - 1].ELEMENT, "class", function(res) {
										temp.verify.notEqual(res.value.indexOf("disable-row"), -1, 'Failed to toggle the score for the specified row')
									})
								})
						})
					})
			} catch (err) {
				logger.error(err.stack)
			}
			return this
		},

		addFeedbackText: function(feedbackText, rowNumber) {
			var temp = this
			try {
				this
					.api.elements('xpath', '//a[@title="Feedback"]', function(result) {
						temp.api.elementIdClick(result.value[rowNumber - 1].ELEMENT, function() {
							temp
								.waitForElementVisible('@feedbackTextArea', 5000)
								.click('@feedbackTextArea')
								.clearValue('@feedbackTextArea')
								.setValue('@feedbackTextArea', feedbackText)
								.api.useXpath()
								.click('//button[text()="Apply"]')
								.waitForElementPresent('//modal[contains(@class, "feedback")]//div[contains(@style, "display: none")]')
								.useCss()
						})
					})
			} catch (err) {
				logger.error(err.stack)
			}
			return this
		}, 

		addHint: function(hintInputText, penaltyScore, rowNumber) {
			var temp = this
			try {
				this
					.api.elements('xpath', '//a[contains(@title, "Hint Available")]', function(result) {
						temp.api.elementIdClick(result.value[rowNumber - 1].ELEMENT, function() {
							temp
								.waitForElementVisible('@hintInputArea', 5000)
								.click('@hintInputArea')
								.clearValue('@hintInputArea')
								.setValue('@hintInputArea', hintInputText)
								.click('@penaltyInputArea')
								.clearValue('@penaltyInputArea')
								.setValue('@penaltyInputArea', penaltyScore)
								.api.useXpath()
								.click('//div[contains(@style, "display: block")]//button[text()="Done"]')
								.waitForElementPresent('//modal[contains(@class, "hint")]//div[contains(@style, "display: none")]')
								.useCss()
						})
					})
			} catch (err) {
				logger.error(err.stack)
			}
			return this
		},

		addNewRule: function(cellValue, score) {
			try {
				this
					.waitForElementVisible('@addNewRule', 5000)
					.click('@addNewRule')
					.setValue('@addNewCell', cellValue)
					.click('@addNewScore')
					.clearValue('@addNewScore')
					.setValue('@addNewScore', score)
					.api.useXpath()
					.click('//a[@title="Save Rule"]')
					.useCss()
			} catch (err) {
				logger.error(err.stack)
			}
			return this
		}
	}]

};