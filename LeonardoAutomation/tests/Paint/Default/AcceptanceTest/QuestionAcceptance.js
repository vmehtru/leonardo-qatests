var getItemID = require(currentDirPath + '/utils/modules/getID');
var getEmbedID = require(currentDirPath + '/utils/modules/embedUrl');
describe ('Question Item Acceptance Test', function(client) {

	this.timeout(90000000);
	var testfile_name = this.file;
	var suite_name = this.title;
    var testcase_name;
    var ItemId;
    var embedID;

	before(function(client, done) {

		logger.info("Executing File: " + testfile_name + ".js")
		logger.info("Starting following suite: " + suite_name)
		
		loginPage = client.page.loginPage();
		createItem = client.page.createItem();
		setupPage = client.page.setupPage();
		templatePage = client.page.templatePage();
		instructionsPage = client.page.instructionsPage();
		preferencePage = client.page.preferencePage();
		documentUploadPage = client.page.documentUpload();
		scoringRulesPage = client.page.scoringRulesPage();
		previewPublishPage = client.page.previewPublishPage();
     	headerFooterPage = client.page.headerFooterPage();
     	dashBoardPage = client.page.dashBoardPage();
     	embedPlayer = client.page.embedPlayer();
		
		logger.info("Launching Browser: " + client.options.desiredCapabilities.browserName)
		logger.info("Launching the Leonardo Paint URL: " + data[argv.testEnv].url)

    	client
    		.maximizeWindow()
            .url(data[argv.testEnv].url, function() {
             loginPage
                    .loginWithDetails(data[argv.testEnv].org, data[argv.testEnv].username, data[argv.testEnv].password)
                    .verify.elementPresent(headerFooterPage.elements.title.selector, "Login is not successful")
            })
            .waitUntilLoaderPresent(function() {
                done();
          })
	});

	beforeEach(function(client, done) {
        testcase_name = this.currentTest.title;
        logger.info("Executing following Test Case: " + testcase_name)
        done();
    });

	it('TC01 - Question Workflow Validation using workbook having multiple sheets', function(client, done) {
		var title = "Question-Item - With Multiple Sheets"
		client
			.waitUntilLoaderPresent(function() {
				createItem
					.openCreate()
					.verify.containsText(createItem.elements.createPageHeader.selector, 'Create a New Leonardo Item', 'Unable to open Create page')
                    .verify.cssClassPresent(createItem.elements.presetationItem.selector, 'active', 'Presentation Item is Not Active')  
					.createItem(title, "question");
			})
			.waitUntilLoaderPresent(function() {
				setupPage
					.verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active") 
                    .verify.value('#title', title, 'SetUp Page Title is not Correct')
					.enterSetupDetails("This is the description", "Other Tags")
					.verify.containsText('#other div.tag-sel-item', "Other Tags", "Tag not entered correctly")
					.click(properties.get("nextButton"))
			})
			
			.waitUntilLoaderPresent(function() {
				templatePage
					.verify.containsText(properties.get("activeTabText"), "Template", "Template Tab is not active") 
					.verify.cssClassPresent(templatePage.getSelectorForTemplate(1), "template-selected", "Default Template is not selected")
					.verify.cssClassPresent(templatePage.getSelectorForLayout(1), "layout-selected", "Default Layout is not selected")
					.chooseTemplate(1)
					.chooseLayout(2)
					.click(properties.get("nextButton"));

			})
			
			.waitUntilLoaderPresent(function() {
				instructionsPage
					.verify.containsText(properties.get("activeTabText"), "Instructions", "Instructions Tab is not active") 
					.clickRow(1)
					.enterInstructionText("Instruction 1 Text")
					.clickRow(2)
					.enterInstructionText("Instruction 2 Text")
					.clickRow(3)
					.enterInstructionText("Instruction 3 Text")
					.selectTextForRow(2)
					.selectFormattingToolbarOption(1)
					.verify.elementPresent('div.content-area p:nth-child(2) b')
					.selectFormattingToolbarOption(3)
					.verify.elementPresent('div.content-area p:nth-child(2) u')
					.clickRow(4)
					.addSubItem()
					.selectSubItemOption(1)
			})
			
			.waitUntilLoaderPresent(function() {
				instructionsPage
					.api.frame(0)
					.waitForElementVisible(properties.get("nextButton"))
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function(){
		        documentUploadPage
		          .uploadDocument('Motion_Profile.xlsx')
		          .click(properties.get("nextButton"))
		      })
		        
		    .waitUntilLoaderPresent(function(){
		        preferencePage
		          .waitForElementPresent('div.leo-presentationwidget', 10000)
		          .selectSheet("Building Profile")
		          .cellRange("A5","E10")
		          .enableToggleButton("Formula Bar")
		          .click(properties.get("nextButton"))
		      })      
		        
		    .waitUntilLoaderPresent(function() {
		        previewPublishPage
		          .validatePublishCheckpoints()
		          .verify.cssClassPresent('button.publish-btn', 'disabled', 'Publish button is not disabled in the Embed Item')
		      }) 

		    .click(properties.get("finishButton"))
			.frameParent()
			.frame(0)
		    .waitForElementVisible('.leohost .k-spreadsheet-active-cell div', 10000)
			.verify.containsText('.leohost .k-spreadsheet-active-cell div', 'Cruise', 'Text inside the Embedded Item is not visible')
			.verify.cssClassPresent('.leohost .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
			.frameParent()
			.verify.elementPresent('div.embedded-preview-container', "Embedded item not displayed correctly")
			.click(properties.get("nextButton"))
		    .waitUntilLoaderPresent(function() {
				documentUploadPage
					.verify.containsText(properties.get("activeTabText"), "Documents", "Documents Tab is not active") 
					.uploadDocument("MultiSheet_Start.xlsx", "MultiSheet_Final.xlsx")
					.verify.containsText(documentUploadPage.elements.submissionStatus1.selector, String("Uploaded on").trim())
					.openPreviewDocument('1')
                    .verify.containsText(documentUploadPage.elements.previewWindowHeader.selector,'Initial Document')
                    .verify.containsText(documentUploadPage.elements.activeCellData.selector,'400')
                    .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled')
                    .closePreviewDocument()
                    .openPreviewDocument('2')
                    .verify.containsText(documentUploadPage.elements.previewWindowHeader.selector,'Final Document')
                    .verify.containsText(documentUploadPage.elements.activeCellData.selector,'500')
                    .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled')
                    .closePreviewDocument()
					.click(properties.get("nextButton"));
			})
			
			.waitUntilLoaderPresent(function() {
				scoringRulesPage
					.verify.containsText(properties.get("activeTabText"), "Scoring Rules", "Scoring Rules Tab is not active") 
					.toggleScore(2)
					.verify.value("div.score-heading  input", "92", "Score is not matching")
					.addFeedbackText("Feedback updated", 5)
					.verify.elementCount('i.active-action', 1, "Feedback text not added successfully")
					.addHint("This is the hint text", 2, 8)
					.verify.elementCount('i.active-action', 2, "Hint not added successfully")
					.addNewRule("A4", 5)
					.verify.elementCount("div.table-row", 13, "New Rule not added successfully")
					.click(scoringRulesPage.elements.initialDocument.selector)
					.waitForElementVisible(documentUploadPage.elements.previewWindowHeader.selector, 5000)
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector,'Initial Document')
                    .verify.containsText(documentUploadPage.elements.activeCellData.selector,'400')
                    .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled');
				documentUploadPage
					.closePreviewDocument()	                   
					.click(scoringRulesPage.elements.finalDocument.selector)
					.waitForElementVisible(documentUploadPage.elements.previewWindowHeader.selector, 5000)
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector,'Final Document')
                    .verify.containsText(documentUploadPage.elements.activeCellData.selector,'500')
                    .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled')
					.closePreviewDocument()	
					.click(properties.get("previewButton"));
				previewPublishPage
					.validateTextForRow("Instruction 1 Text", 1)
					.closePreview()
					.click(properties.get("nextButton"));
			})
			
			.waitUntilLoaderPresent(function() {
				previewPublishPage
					.verify.containsText(properties.get("activeTabText"), "Preview & Publish", "Preview & Publish Tab is not active") 
					.validatePublishCheckpoints()
					.openPreview()
					.validateTextForRow("Instruction 3 Text", 3)
					.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item');
				embedPlayer
    				.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
    				.verify.containsText('.leo-canvasarea .k-spreadsheet-active-cell div', '400', 'Text is not correct inside the Player window')
    				.waitForElementVisible(properties.get('showHint'), 5000)
					.click(properties.get('showHint'))
					.verify.elementCount(embedPlayer.elements.activeHintCells.selector, 1)
					.selectCell('A2')
					.waitForElementVisible(embedPlayer.elements.remainingHintsCount.selector, 5000)
					.verify.containsText(embedPlayer.elements.remainingHintsCount.selector, '1')
					.click(embedPlayer.elements.revealHint.selector)
					.verify.elementNotPresent(embedPlayer.elements.remainingHintsCount.selector)
					.validateHintTextForCell('A2', 1, "This is the hint text")
					.validatePenaltyForCell('A2', 1, 2)
					.click(embedPlayer.elements.closeHintContainer.selector)
					.click(properties.get('checkMyWorkButton'))
					.selectCell('A1')
					.validateTooltipForCell('A1', 'Feedback updated', 1)
					.validateTooltipForCell('A1', 'Bold should be applied.', 2);
				previewPublishPage	
					.closePreview()
					.publishItem()
					.api.useXpath()
					.verify.containsText(previewPublishPage.elements.itemState.selector, "Published", "Item failed to publish")
					.verify.containsText(previewPublishPage.elements.publishedID.selector, "leo-leonardo-dev", "Item failed to publish")
					.useCss();
					
				getItemID
					.getLeonardoID(client)
                    .then(function(value) {
                    	logger.info("Item ID: " + value);
                    	ItemId = value
                    })
			})	
			.click(properties.get("finishButton"))
			.waitUntilLoaderPresent(function() {
	            dashBoardPage
	                .openItemPreview(ItemId)
	                .waitForElementPresent(dashBoardPage.elements.previewQuestionActiveCellData.selector, 5000)
	                .verify.containsText(dashBoardPage.elements.previewQuestionActiveCellData.selector, '400', 'Text is not visible')
					.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
	            previewPublishPage
	            	.closePreview()
	        })

	        .perform(function() {
	        	dashBoardPage
	        		.openEmbed(ItemId)
       			getEmbedID
	        		.getEmbedUrl(client)
                    .then(function(value) {
                    	logger.info("Embed Item ID: " + value);
                    	client.url(value, function() {
                    		client.waitForElementVisible('div.leo-widget', 10000)
                    	})
                    })
                    
            })

            .perform(function() {
            	embedPlayer
    				.waitForElementVisible('div.leo-widget', 10000)
    				.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
    				.verify.containsText('.leo-canvasarea .k-spreadsheet-active-cell div', '400', 'Text is not correct inside the Player window')
    				.waitForElementVisible(properties.get('showHint'), 5000)
					.click(properties.get('showHint'))
					.verify.elementCount(embedPlayer.elements.activeHintCells.selector, 1)
					.selectCell('A2')
					.waitForElementVisible(embedPlayer.elements.remainingHintsCount.selector, 5000)
					.verify.containsText(embedPlayer.elements.remainingHintsCount.selector, '1')
					.click(embedPlayer.elements.revealHint.selector)
					.verify.elementNotPresent(embedPlayer.elements.remainingHintsCount.selector)
					.validateHintTextForCell('A2', 1, "This is the hint text")
					.validatePenaltyForCell('A2', 1, 2)
					.click(embedPlayer.elements.closeHintContainer.selector)
					.click(properties.get('checkMyWorkButton'))
					.selectCell('A1')
					.validateTooltipForCell('A1', 'Feedback updated', 1)
					.validateTooltipForCell('A1', 'Bold should be applied.', 2)
            })
	});

	it('TC02 - Question Workflow Validation using workbook having single sheet', function(client) {
		var title = "Question-Item - With Single Sheet"
		client
			.url(data[argv.testEnv].url)
			.waitUntilLoaderPresent(function() {
				createItem
					.openCreate()
					.verify.containsText(createItem.elements.createPageHeader.selector, 'Create a New Leonardo Item', 'Unable to open Create page')
                    .verify.cssClassPresent(createItem.elements.presetationItem.selector, 'active', 'Presentation Item is Not Active')  
					.createItem(title, "question");
			})
			.waitUntilLoaderPresent(function() {
				setupPage
					.verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active") 
                    .verify.value('#title', title, 'SetUp Page Title is not Correct')
					.enterSetupDetails("This is the description", "Other Tags")
					.click(properties.get("nextButton"))
			})
			
			.waitUntilLoaderPresent(function() {
				templatePage
					.verify.containsText(properties.get("activeTabText"), "Template", "Template Tab is not active") 
					.verify.cssClassPresent(templatePage.getSelectorForTemplate(1), "template-selected", "Default Template is not selected")
					.verify.cssClassPresent(templatePage.getSelectorForLayout(1), "layout-selected", "Default Layout is not selected")
					.chooseTemplate(2)
					.click(properties.get("nextButton"))
			})
			
			.waitUntilLoaderPresent(function() {
				instructionsPage
					.verify.containsText(properties.get("activeTabText"), "Instructions", "Instructions Tab is not active") 
					.clickRow(1)
					.enterInstructionText("Instruction 1 Text")
					.clickRow(2)
					.enterInstructionText("Instruction 2 Text")
					.clickRow(3)
					.enterInstructionText("Instruction 3 Text")
					.selectTextForRow(2)
					.selectFormattingToolbarOption(1)
					.verify.elementPresent('div.content-area p:nth-child(2) b')
					.selectFormattingToolbarOption(3)
					.verify.elementPresent('div.content-area p:nth-child(2) u')
					.clickRow(3)
					.click(properties.get("nextButton"))
			})
			
			.waitUntilLoaderPresent(function() {
				documentUploadPage
					.verify.containsText(properties.get("activeTabText"), "Documents", "Documents Tab is not active") 
					.uploadDocument("testfile.xlsx", "testfile_Final.xlsx")
					.verify.containsText(documentUploadPage.elements.submissionStatus1.selector, String("Uploaded on").trim())
					.openPreviewDocument('1')
                    .verify.containsText(documentUploadPage.elements.previewWindowHeader.selector,'Initial Document')
                    .verify.containsText(documentUploadPage.elements.activeCellData.selector,'1')
                    .closePreviewDocument()
                    .openPreviewDocument('2')
                    .verify.containsText(documentUploadPage.elements.previewWindowHeader.selector,'Final Document')
                    .verify.containsText(documentUploadPage.elements.activeCellData.selector,'1')
                    .closePreviewDocument()
                    .click(properties.get("nextButton"))
			})
			
			.waitUntilLoaderPresent(function() {
				scoringRulesPage
					.verify.containsText(properties.get("activeTabText"), "Scoring Rules", "Scoring Rules Tab is not active") 
					.toggleScore(1)
					.verify.value("div.score-heading  input", "80", "Score is not matching")
					.click(properties.get("previewButton"));
				previewPublishPage
					.validateTextForRow("Instruction 1 Text", 1)
					.closePreview()
					.click(properties.get("nextButton"))
			})
			
			.waitUntilLoaderPresent(function() {
				previewPublishPage
					.verify.containsText(properties.get("activeTabText"), "Preview & Publish", "Preview & Publish Tab is not active") 
					.validatePublishCheckpoints()
					.openPreview()
					.validateTextForRow("Instruction 3 Text", 3)
					.closePreview()
					.publishItem()
					.api.useXpath()
					.verify.containsText(previewPublishPage.elements.itemState.selector, "Published", "Item failed to publish")
					.verify.containsText(previewPublishPage.elements.publishedID.selector, "leo-leonardo-dev", "Item failed to publish")
					.useCss();

				getItemID
					.getLeonardoID(client)
                    .then(function(value) {
                    		logger.info("Item ID:"+value);
                    		ItemId=value
                    	})
			})	
			.click(properties.get("finishButton"))
			.waitUntilLoaderPresent(function() {
	            dashBoardPage
	                .openItemPreview(ItemId)
	                .waitForElementPresent(dashBoardPage.elements.previewQuestionActiveCellData.selector, 5000)
	                .verify.containsText(dashBoardPage.elements.previewQuestionActiveCellData.selector, '1', 'Text is not visible');
	            previewPublishPage
	            	.closePreview()
	        }) 
	});

	afterEach(function (client, done) {
        testcase_name = this.currentTest.title;
        logger.info("Completed following Test Case: " + testcase_name)
        done();
    });

	after(function(client, done) {
		logger.info("Completed following suite : " + suite_name)
    	client.end(function() {
      		done();
    	});
  	});
});