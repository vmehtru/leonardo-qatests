'use strict'

exports.getEmbedUrl = function(self) {
    var embedData;
    var embedUrl;

    return new Promise(function(resolve, reject) {
        self.waitForElementVisible('.modal-embed .modal-header', 10000)
            .getValue('.modal-embed textarea', function(result) {
                embedData = result.value
                var embedValue = embedData.substring(embedData.lastIndexOf("=") + 1)
                embedUrl = embedValue.substring(0, embedValue.indexOf(">"))
                embedUrl = embedUrl.split('\"').join('')
                resolve(embedUrl);
            })      
    })
} 
