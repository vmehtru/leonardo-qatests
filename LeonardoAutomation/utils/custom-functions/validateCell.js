var util = require('util');
var events = require('events');
var fs = require('fs');
const assert = require('assert');

var diff = require('deep-diff');
var syncLoop = require('../../utils/modules/syncLoop.js')
var elementSelector = "div.k-spreadsheet-cell.k-spreadsheet-active-cell";
var elementSelector1= "div.k-spreadsheet-cell.k-spreadsheet-active-cell>div";
var formulabarvalue= ".k-spreadsheet-formula-bar>.k-spreadsheet-formula-input";
var defaultCellValue= "";
var isVisibleFlag;
var elementSelectorExists;

function iterationCopy(src) {
    let target = {};
    for (let prop in src) {
        if (src.hasOwnProperty(prop)) {
            target[prop] = src[prop];
        }
    }
    return target;
}


function Validate() {
    events.EventEmitter.call(this);
}


util.inherits(Validate, events.EventEmitter);


Validate.prototype.command = function(activeCells, target, property, expected, precheckEvaluation, callback) {
    flag = true;

    var self = this;
    var cellsToSelect = activeCells.split(",");
    //console.log(cellsToSelect);
    var existingCellData = {};
	var colNum,rowNum;

    syncLoop.loopExecution(cellsToSelect.length, function(loop) {

            var appendData = {};
            var itr = loop.iteration();
			var res = cellsToSelect[itr].substr(0, 1);
			var colNum = res.charCodeAt(0);
			var rowNum = cellsToSelect[itr].replace(/[^0-9]/g, '');
			// change alphabet to index number
			if (colNum <= 89 && colNum >= 64) {
				colNum = colNum - 64;
			}
			
			elementSelector = "div.leo-canvasarea div.k-spreadsheet-cell.k-row-" + (rowNum - 1) + ".k-col-" + (colNum - 1);
			elementSelector1 = "div.leo-canvasarea div.k-spreadsheet-cell.k-row-" + (rowNum - 1) + ".k-col-" + (colNum - 1) + ">div";

			self.api.element('css selector', elementSelector, function(result) {
				elementSelectorExists = result.status;
				if (1 < cellsToSelect.length && 0 == elementSelectorExists) {
					self.api.selectCell(cellsToSelect[itr])
				}
				//})
			
				if ("getCssProperty" === target && 0 == elementSelectorExists) {
					self.api.getCssProperty(elementSelector, property, function(result) {
						console.log("Validate CSS Property of Cell : "+ cellsToSelect[itr]);
							console.log("CSS Property Value Found:" + result.value);
							console.log("CSS Property Value Expected:" + expected);
						self.api.verify.equal(expected, result.value);
						//console.log("Validation ends for Cell : "+ cellsToSelect[itr]);
						loop.next();
					})
				}

				else if ("getText" === target) {
					self.api.element('css selector', elementSelector1, function(result) {
						isVisibleFlag = result.status;
						if (0 == isVisibleFlag){
							self.api.getText(elementSelector1, function(result) {
								console.log("Validate Cell Text of Cell : "+ cellsToSelect[itr]);
								console.log("Cell Text Found:" + result.value);
								console.log("Cell Text Expected:" + expected);
								self.api.verify.equal(expected, result.value);
								//console.log("Validation ends for Cell : "+ cellsToSelect[itr]);
							})
						}
						else {
							console.log("Validate Cell Text of Cell : "+ cellsToSelect[itr]);
							console.log("Cell Text Found:" + defaultCellValue);
							console.log("Cell Text Expected:" + expected);
							self.api.verify.equal(expected, defaultCellValue);
							//console.log("Validation ends for Cell : "+ cellsToSelect[itr]);
						}
						loop.next();
					})
				}

            else if ("getAttribute" === target && 0 == elementSelectorExists) {

               /* if (expected.includes('vertical-align')) {
                    elementSelector = "div.k-spreadsheet-cell.k-spreadsheet-active-cell > div";
                } else
                    elementSelector = "div.k-spreadsheet-cell.k-spreadsheet-active-cell";*/
                var regex = /\-[a-z]{0,1}/g;
				
                self.api.getAttribute(elementSelector, property, function(result) {
						console.log("Validate Attribute Value of Cell : "+ cellsToSelect[itr]);
                        console.log("Attribute Value Found:" + result.value);
                        console.log("Attribute Value Expected:" + expected);

                        var resToCheck = result.value;

                        var defaultMessage = "Expected" + expected.trim() + " but got :-" + resToCheck;

                        self.api.verify.equal(resToCheck.includes(expected.trim()), true);
						//console.log("Validation ends for Cell : "+ cellsToSelect[itr]);

                        // })
                        // .perform(function(){

                        if (precheckEvaluation === false) {

                            //console.log("precheck has not been initialized");
                            loop.next();

                        }

                        else
                        {       
                            var objectTODuplicate = {};
                            

                            if(flag === true)
                            {

                            if ((fs.existsSync('./temp/precheck.json'))) {
                                flag = false;

                                if (!(fs.existsSync('./temp/precheckDuplicate.json'))) {


                                var preCellState = fs.readFileSync('./temp/precheck.json');
                                cellData = JSON.parse(preCellState);

                                existingCellData = cellData;

                                console.log("Creating Duplicate Precheck")
                                fs.writeFileSync('./temp/precheckDuplicate.json', JSON.stringify(cellData , null, 4), 'utf-8')

                            }

                            else{

                                var preCellState = fs.readFileSync('./temp/precheckDuplicate.json');
                                cellData = JSON.parse(preCellState);

                                existingCellData = cellData;

                                }
                            }

                            }

                        }

                    })
                    .perform(function() {
                         

                        if (precheckEvaluation === true) {

                            if(Object.keys(existingCellData).length != 0)
                            {

                          //  if (fs.existsSync('./temp/precheckDuplicate.json')) {


                               //update the object if precheck duplicate json already exists
                                // duplicateCellDataExists = fs.readFileSync('./temp/precheckDuplicate.json');
                                // existingCellData = JSON.parse(duplicateCellDataExists);

                                cellStateToBeUpdated = existingCellData[cellsToSelect[itr]];

                               
                              //  console.log("Cell To be updated : " + JSON.stringify(cellStateToBeUpdated));

                                objectToBeUpdated = iterationCopy(cellStateToBeUpdated); 
                               // console.log("objectToBeUpdated" + JSON.stringify(objectToBeUpdated))                   

                                
                                //check if the property already exists in the precheck duplicate json
                                if (objectToBeUpdated.hasOwnProperty(property)) {

                                      objectToBeUpdated[property] = expected;
                                   }


                                //if property is style then update the object
                                else if (property === "style") {

                                    newProperty = expected.split(":");

                                    newProperty[0] = newProperty[0].replace(regex, function(match) {
                                        return match.toUpperCase().replace("-", "");
                                    })

                                    objectToBeUpdated[newProperty[0]] = newProperty[1].trim();

                                }

                                //if none of the above(a new property needs to be appended)
                                else

                                { 
                                    objectToBeUpdated[property] = expected.trim();
                                }
                                   
                                existingCellData[cellsToSelect[itr]] = objectToBeUpdated;

                                 console.log("Appending Data in Precheck Duplicate ")
                                 //+ JSON.stringify(existingCellData));
                                 loop.next();

                              //  fs.writeFileSync('./temp/precheckDuplicate.json', JSON.stringify(appendData, null, 4), 'utf-8');   

                                }

                            // }
                        } 

                        else {

                             //console.log("Skipping Precheck Duplicate Creation");
                             loop.next();
                        }

                    })
                    // .perform(function(){
                       
                        
                    //         loop.next();


                    //     // else
                    //     //     
                    // })
                    // .perform(function() {
                    //     if (precheckEvaluation === true) {
                    //         if ((fs.existsSync('./temp/precheckDuplicate.json'))) {

                    //             self.api.postCheck(cellsToSelect[itr], function() {
                    //                 console.log("POST JSON populated")
                    //                 loop.next();
                    //             })


                    //         }
                    //     } else {
                    //         console.log("POST JSON Skipping");
                    //         loop.next();
                    //     }


                    // });
				//console.log("Validation ends for Cell : "+ cellsToSelect[itr]);
            }
			else if(-1 == elementSelectorExists){
					console.log("ERROR!! Selector does not exist for Cell: "+ cellsToSelect[itr] + " (" + elementSelector + ")");
					self.api.verify.equal("ERROR!! Selector does not exist for Cell: "+ cellsToSelect[itr], "");
					loop.next();
			}
		})
     },
     function() {
		
		if (precheckEvaluation === true) {
			fs.writeFileSync('./temp/precheckDuplicate.json', JSON.stringify(existingCellData, null, 4), 'utf-8')
            //,function(){
				console.log("Writing..");
            }          
           
            if (typeof(callback) === "function") {
                callback.call(self.client.api);
            }

            //console.log("End of validations");
            self.emit('complete');
            return this;

    });

};

module.exports = Validate;