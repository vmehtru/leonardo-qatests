var util = require('util');
var events = require('events');

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

// var elementSelector = "div.leo-canvasarea div.k-spreadsheet-cell-editor.k-spreadsheet-formula-input";

//div.k-spreadsheet-cell.k-spreadsheet-active-cell
function enterText() {
    events.EventEmitter.call(this);
}


util.inherits(enterText, events.EventEmitter);


enterText.prototype.command = function(elementName, text, cb) {

    var self = this;
    var elementSelector;
    //console.log(text);
    self.api.url(function(result) {
        if (result.value.includes('embedframe')) {
            elementSelector = "div.leo-canvasarea div.k-spreadsheet-cell-editor.k-spreadsheet-formula-input";
        } else {
            elementSelector = "div.k-spreadsheet-cell-editor.k-spreadsheet-formula-input";
        }
        if (!elementName) {
            self.api.doubleClick();
        }
        self.api.setValue(elementName ? properties.get(elementName) : elementSelector, String(text), function() {

            if (typeof(cb) === "function") {
                cb.call(self.client.api);
            }

            self.emit('complete');
            return this;

        });
        // self.api.getElementSize('k-spreadsheet-cell k-spreadsheet-active-cell', (result) => {
        //     console.log(`Currently ${JSON.stringify(result.value)} is active`);
        // });

    })

};

module.exports = enterText;