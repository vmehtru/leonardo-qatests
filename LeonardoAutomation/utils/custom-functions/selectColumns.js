var util = require('util');
var events = require('events');
var fs = require('fs');

var calculateDimensions = require('./../modules/getDimensions');


function selectColumns() {

    events.EventEmitter.call(this);
}


util.inherits(selectColumns, events.EventEmitter);

selectColumns.prototype.command = function(range, cb) {

    var dragtarget = [];
    var droptarget = [];
    var offset = [];
    var cellRange = String(range).split(":");

    var self = this;

    function offsets(cellName) {

        var colNum;

        //rowNum = cellName.replace(/[^0-9]/g, '');
        colNum = cellName.charCodeAt(0);

        if (colNum <= 89 && colNum >= 64) {
            colNum = colNum - 64;
        }
        console.log('ColNum ' + colNum)

        return new Promise(function(resolve, reject) {

            getHeight(colNum, self).then(function(result) {
                offset[1] = result;

                calculateDimensions.getWidth(colNum, self).then(function(result1) {
                    offset[0] = result1;
                    resolve(offset);
                })

            })
    })
}

    var promise1 = new Promise(function(resolve, reject) {

        offsets(cellRange[0]).then(function(value) {
            resolve(value);
        });
    })


    var promise2 = new Promise(function(resolve, reject) {

        offsets(cellRange[1]).then(function(result) {
            resolve(result);
        });
    })



    async function runSerial() {
        var draggablesrc = [];
        var draggableto = [];

        
        const executePromise1 = await promise1;

        draggablesrc[0] = executePromise1[0];
        draggablesrc[1] = executePromise1[1];

        const executePromise2 = await promise2;
       
        draggableto[0] = executePromise2[0];
        draggableto[1] = executePromise2[1];


        self.api.moveToElement("body", draggablesrc[0], draggablesrc[1])
            .mouseButtonDown(0)
            .moveToElement("body", draggableto[0], draggableto[1])
            .mouseButtonUp(0, function() {

                if (typeof(cb) === "function") {
                    cb.call(self.client.api);
                }
                self.emit('complete');
            })
    }
    runSerial();
    return this;

};

getHeight = function(colNum,self) {

    // var self = this;
    var hgt;
    var colpath;
    return new Promise(function(resolve, reject) {
        self.api.url(function(current_url) {
            if(current_url.value.includes('embedframe')) {
                colpath = "div.leo-canvasarea > div.widgetContainer > div.spreadsheet.k-widget.k-spreadsheet > div.k-spreadsheet-view > div.k-spreadsheet-fixed-container > div.k-spreadsheet-pane.k-top.k-left > div.k-spreadsheet-column-header > div:nth-child(" + colNum + ") > div"            
            } else {
                colpath = "div.widgetContainer > div.spreadsheet.k-widget.k-spreadsheet > div.k-spreadsheet-view > div.k-spreadsheet-fixed-container > div.k-spreadsheet-pane.k-top.k-left > div.k-spreadsheet-column-header > div:nth-child(" + colNum + ") > div"
            }
            self.api.getElementSize(colpath, function(result) {
                hgt = ((result.value.height) / 2);
            })

            self.api.getLocationInView(colpath, function(result) {
                hgt = hgt + result.value.y;
                resolve(hgt);
            })
        })
    })

}

module.exports = selectColumns;