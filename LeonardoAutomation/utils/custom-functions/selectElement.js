
var util = require('util');
var events = require('events');
var fs = require('fs');
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

function selectElement() {
	
events.EventEmitter.call(this);

}


util.inherits(selectElement, events.EventEmitter);

selectElement.prototype.command = function(propertyName, callback) {

	//timeoutInMilliseconds=5000;
   //console.log(propertyName)
    var self = this;
	self.api
			.waitForElementVisible(properties.get(propertyName), 5000, 500, true, function() {}, "!!ERROR: " + propertyName + " was not visible within 5 secs.")
			.click(properties.get(propertyName)) 
			.pause((100),function(){
				if(typeof (callback) === "function") {
					callback.call(self.client.api);
					//console.log("Element " + propertyName + " clicked");
				}
				self.emit('complete');
				return this;
			})    
};

module.exports = selectElement;



